install dependencies
~~~~~~~~~~~~~~~~~~~~

run this command in the directory ``searx/static/themes/priza``

``npm install``

compile sources
~~~~~~~~~~~~~~~

run this command in the directory ``searx/static/themes/priza``

``grunt``

or in the root directory:

``make grunt``
